Version 1.0 :

	- Ajout du diaporama des différentes catégories (Java/MySQL/Web/Python)
	- Création des catégories et de leurs design
	- Ajout d'un projet "Convertisseur_Python" ainsi que le design des pages de projets
	- Ajout d'un diaporama dans la navbar afin de mieux naviguer sur le site dans les catégories et les projets
	- Modification du style du diaporama dans les catégories et les pages de projets, désormais le slider est de bas en haut au lieu de gauche à droite
	- Ajout du projet 'Colipays' dans la catégorie Java
	- Ajout d'un bouton pour accéder à la page about
	- Création de la page About
	- Ajout d'un copyright
	- Rectification des feuilles de styles
	- Rectification du copyright
	- Ajout d'une page BDD.php afin de montrer un exemple de connexion SQL via php
	- Ajout d'une page de maintenance
	- Rectification de la page BDD.php désormais on peut afficher une news à partir de la BDD tout en pouvant l'actualiser en directe avec du code HTML
	- Page  BDD.php renommé en news.php