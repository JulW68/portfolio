<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<!--<meta http-equiv="refresh" content="1; URL=maintenance.html" />-->
	<title>Portfolio Julien Wimmer</title>
	<!-- css -->
	<link rel="stylesheet" href="css/style_table.css">
</head>
<body>
<header>
	<a href="index.html#sl_i1"><img align="left" class="banner" id="top" src="img/Header2.png" alt="btn-accueil" width="8%" height="8%"></a>
</header>
	<br>
	<br>
	<br>
	<br>
	<br><br><br><br><br><br>
<section class ="news">
<?php
try
{
	// On se connecte à MySQL
	$bdd = new PDO('mysql:host=localhost;dbname=php;charset=utf8', 'root', '');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

// Si tout va bien, on peut continuer

// On récupère tout le contenu de la table jeux_video
$reponse = $bdd->query('SELECT * FROM news');
?>
 <table>
                <tr>
                    <th> Auteur </th>
                    <th> News </th>
					<th> Date </th>
                </tr>
<?php
//On affiche les lignes du tableau une à une à l'aide d'une boucle
while($donnees = $reponse->fetch())
{
            ?>
                <tr>
                    <th><?php echo $donnees['Auteur'];?></th>
                    <th><?php echo $donnees['Contenue'];?></th>
					<th><?php echo $donnees['Date'];?></th>
                </tr>
<?php
}

$reponse->closeCursor(); // Termine le traitement de la requête

?>
        </table>
	</section>
</body>
<br><br><br><br><br><br><br><br><br><br><br>
<footer>
<br>
	<a href="https://gitlab.com/JulW68/portfolio"><img align="left" class="github" id="top" src="img/Footer2.png" alt="btn-git" width="10%" height="10%"></a>
	<a target="_blank" align="center">Copyright © 2020 Julien Wimmer - Tous droits réservés</a> 
	<a href="about.html#sl_i1"><img align="right" class="aboutus" id="top" src="img/about.png" alt="btn-git" width="10%" height="10%"></a>
</footer>